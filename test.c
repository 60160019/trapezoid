#include <stdio.h>
#include <stdlib.h>
#include <time.h>
// Intergrated Func;
double f(double x)
{
    return x * x;
}

void main()
{
    double a, b, h, sum = 0, approx, x_i;
    int n;
    clock_t time;
    double usageTime;
    time = clock();
    printf("Enter A and B : ");
    scanf("%lf %lf", &a, &b);
    printf("\nEnter sub-intervals : ");
    scanf("%d", &n);

    h = (b - a) / n;
    approx = (f(a) + f(b)) / 2.0;
    for (int i = 1; i <= n - 1; i++)
    {
        x_i = a + i * h;
        approx += f(x_i);
    }
    approx = h * approx;

    printf("Intergral is : %lf\n", approx);
    printf("Usage Time : %lf\n", (double)(clock() - time) / CLOCKS_PER_SEC);
}
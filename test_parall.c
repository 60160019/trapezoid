#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <mpi.h>

double f(double x)
{
    return x * x;
}

double Trap(
    double left_endpt,
    double right_endpt,
    int trap_count,
    double base_len)
{
    double estimate, x;
    int i;
    estimate = (f(left_endpt) + f(right_endpt)) / 2.0;
    for (i = 1; i <= trap_count - 1; i++)
    {
        x = left_endpt + i * base_len;
        estimate += f(x);
    }
    estimate = base_len * estimate;
    return estimate;
}
void main(int argc, char *argv[])
{
    double a;
    double b;
    int n;
    int rank, comm_size, local_n;
    double h, local_a, local_b;
    double local_int, total_int;
    int source;
    double maxtime, mintime, avgtime, runtime;

    if (argc < 4)
    {
        printf("Error enter with this format ./test_parall a b n\n");
        exit(0);
    }
    else
    {
        a = strtod(argv[1], NULL);
        b = strtod(argv[2], NULL);
        n = strtol(argv[3], NULL, 10);
    }

    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);

    h = (b - a) / n;
    local_n = n / comm_size;
    local_a = a + rank * local_n * h;
    local_b = local_a + local_n * h;
    local_int = Trap(local_a, local_b, local_n, h);

    if (rank != 0)
    {
        MPI_Send(&local_int, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }
    else
    {
        total_int = local_int;
        for (source = 1; source < comm_size; source++)
        {
            MPI_Recv(&local_int, 1, MPI_DOUBLE, source, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            total_int += local_int;
        }
    }
    MPI_Finalize();
    // out of parallelized

    if (rank == 0)
    {
        printf("With n = %d trapzoids, our estimate ", n);
        printf("of the interal from %f to %f = %lf\n", a, b, total_int);
        // printf("\n Min : %lf \n Max : %lf \n avg : %lf \n Usage Time = %f\n", mintime, maxtime, avgtime, runtime);
    }
}